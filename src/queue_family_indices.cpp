/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "queue_family_indices.hpp"

namespace zelus
{
  bool
  queue_family_indices::is_complete ()
  {
    return graphics_family.has_value () && present_family.has_value ();
  }
}
