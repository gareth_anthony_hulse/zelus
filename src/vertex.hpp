/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vulkan/vulkan.hpp>
#include <glm/glm.hpp>
#include <array>

namespace zelus
{
  struct vertex
  {
    glm::vec3 pos;
    glm::vec3 color;
    glm::vec2 tex_coord;

    static vk::VertexInputBindingDescription get_binding_description ();
    static std::array<vk::VertexInputAttributeDescription, 3> get_attribute_descriptions ();
      
    bool operator== (const vertex& test_vertex) const;
  };
}
