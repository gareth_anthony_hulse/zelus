/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#define GLFW_INCLUDE_NONE
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include "queue_family_indices.hpp"
#include "swap_chain_support_details.hpp"
#include "uniform_buffer_object.hpp"
#include "texture.hpp"
#include "vertex.hpp"

#include <vulkan/vulkan.hpp>
#include <GLFW/glfw3.h>
#include <cstdio>
#include <functional>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <set>
#include <cstring>
#include <optional>
#include <cstdint>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/hash.hpp>
#include <array>
#include <chrono>
#include <tiny_obj_loader.h>

namespace zelus
{ 
  class render
  {
  public:
    // Functions:
    render ();
    virtual ~render ();
    
    void draw ();
    void create_texture_image ();
    void load_model (std::string& model, std::string& texture, float x_offset, float y_offset, float z_offset);
    
  private:
    // Variables:
    const uint8_t m_max_frames_in_flight = 3;
    
    vk::Instance m_instance;
    vk::PhysicalDevice m_physical_device;
    vk::Device m_logical_device;
    vk::Queue m_graphics_queue;
    vk::Queue m_present_queue;
    VkSurfaceKHR m_surface;
    vk::SwapchainKHR m_swap_chain;
    std::vector<vk::Image> m_swap_chain_images;
    vk::Format m_swap_chain_image_format;
    vk::Extent2D m_swap_chain_extent;
    std::vector<vk::ImageView> m_swap_chain_image_views;
    vk::RenderPass m_render_pass;
    vk::PipelineLayout m_pipeline_layout;
    vk::Pipeline m_graphics_pipeline;
    std::vector<vk::Framebuffer> m_swap_chain_framebuffers;
    vk::CommandPool m_command_pool;
    std::vector<vk::CommandBuffer> m_command_buffers;
    std::vector<vk::Semaphore> m_image_available_semaphores; // TODO: 1
    std::vector<vk::Semaphore> m_render_finished_semaphores;
    std::vector<vk::Fence> m_in_flight_fences;
    std::vector<vk::Fence> m_images_in_flight;
    size_t m_current_frame = 0;
    vk::Buffer m_vertex_buffer;
    vk::DeviceMemory m_vertex_buffer_memory;
    vk::Buffer m_index_buffer;
    vk::DeviceMemory m_index_buffer_memory;
    vk::DescriptorSetLayout m_descriptor_set_layout;
    std::vector<vk::Buffer> m_uniform_buffers;
    std::vector<vk::DeviceMemory> m_uniform_buffers_memory;
    vk::DescriptorPool m_descriptor_pool;
    std::vector<vk::DescriptorSet> m_descriptor_sets;
    vk::Image m_depth_image;
    vk::DeviceMemory m_depth_image_memory;
    vk::ImageView m_depth_image_view;
    vk::SampleCountFlagBits m_msaa_samples = vk::SampleCountFlagBits::e1;
    vk::Image m_color_image;
    vk::DeviceMemory m_color_image_memory;
    vk::ImageView m_color_image_view;
    glm::vec3 m_camera_pos = glm::vec3 (0.0f, 3.5f, 0.0f);
    glm::vec3 m_camera_front = glm::vec3 (0.0f, 0.0f, 0.0f);
    glm::vec3 m_camera_up = glm::vec3 (0.0f, 0.0f, 1.0f);
    double m_yaw = -90.0f;
    double m_pitch = 0.0f;
    double m_last_x;
    double m_last_y;
    bool m_first_cursor = true;
    long double m_time_step = 0.02;
    GLFWwindow* m_window;
    double m_delta_time;
    uint32_t m_image_index;
    bool m_draw_called = false;
    std::vector<std::string> m_image_files;
    std::vector<zelus::texture> m_textures;
    zelus::uniform_buffer_object m_ubo = {};
    std::vector<uint32_t> m_indices;
    std::vector<zelus::vertex> m_vertices;
    
    // Functions:
#ifndef NDEBUG
    bool m_check_validation_layer_support ();
    static VKAPI_ATTR VkBool32 VKAPI_CALL
    m_debug_callback (VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
		       VkDebugUtilsMessageTypeFlagsEXT message_type,
		       const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
		       void* user_data);
    void m_setup_debug_messenger ();
    VkResult m_create_debug_messenger (vk::Instance instance,
					const VkDebugUtilsMessengerCreateInfoEXT* create_info,
					const VkAllocationCallbacks* allocator,
					VkDebugUtilsMessengerEXT* debug_messenger);
    void m_destroy_debug_messenger (vk::Instance instance,
				    VkDebugUtilsMessengerEXT debug_messenger,
				    const VkAllocationCallbacks* allocator);
    void m_populate_debug_messenger_create_info (VkDebugUtilsMessengerCreateInfoEXT& create_info);
#endif // NDEBUG
    
    void m_init_window ();
    void m_init_vulkan ();
    void m_main_loop ();

    void m_create_instance ();
    std::vector<const char*> m_get_required_instance_exts ();
    void m_pick_physical_device ();
    queue_family_indices m_find_queue_families ();
    void m_create_logical_device ();
    void m_create_surface ();
    swap_chain_support_details m_query_swap_chain_support (vk::PhysicalDevice physical_device);
    vk::SurfaceFormatKHR m_choose_swap_surface_format (const std::vector<vk::SurfaceFormatKHR>&
						      available_formats);
    vk::PresentModeKHR m_choose_swap_present_mode (const std::vector<vk::PresentModeKHR>&
						  available_present_modes);
    vk::Extent2D m_choose_swap_extent (const vk::SurfaceCapabilitiesKHR& capabilities);
    void m_create_swap_chain ();
    void m_create_image_views ();
    void m_create_graphics_pipeline ();
    static std::vector<char> m_read_file (const std::string& filename);
    vk::ShaderModule m_create_shader_module (const std::vector<char>& code);
    void m_create_render_pass ();
    void m_create_framebuffers ();
    void m_create_command_pool ();
    void m_create_command_buffers ();
    void m_draw_frame ();
    void m_create_sync_objects ();
    void m_recreate_swap_chain ();
    void m_cleanup_swap_chain ();
    void m_create_vertex_buffer ();
    uint32_t m_find_memory_type (uint32_t type_filter, vk::MemoryPropertyFlags properties);
    void m_create_buffer (vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties,
			   vk::Buffer& buffer, vk::DeviceMemory& buffer_memory);
    void m_copy_buffer (vk::Buffer src_buffer, vk::Buffer dst_buffer, vk::DeviceSize size);
    void m_create_index_buffer ();
    void m_create_descriptor_set_layout ();
    void m_create_uniform_buffers ();
    void m_update_uniform_buffer (uint32_t current_image);
    void m_create_descriptor_pool ();
    void m_create_descriptor_sets ();
    void m_create_image (uint32_t width, uint32_t height, uint32_t mip_levels,
			  vk::SampleCountFlagBits num_samples, vk::Format format, vk::ImageTiling tiling,
			  vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties, vk::Image& image,
			  vk::DeviceMemory& image_memory);
    vk::CommandBuffer m_begin_single_time_commands ();
    void m_end_single_time_commands (vk::CommandBuffer command_buffer);
    void m_copy_buffer_to_image (vk::Buffer buffer, vk::Image image, uint32_t width, uint32_t height);
    void m_transition_image_layout (vk::Image image, vk::Format format, vk::ImageLayout old_layout,
				     vk::ImageLayout new_layout, uint32_t mip_levels);
    vk::ImageView  m_create_image_view (vk::Image image, vk::Format format, vk::ImageAspectFlags aspect_flags,
				       uint32_t mip_levels);
    void m_create_depth_resources ();
    vk::Format m_find_supported_format (const std::vector<vk::Format>& candidates, vk::ImageTiling tiling,
				       vk::FormatFeatureFlags features);
    vk::Format m_find_depth_format ();
    void m_generate_mipmaps (vk::Image image, vk::Format image_format, int32_t tex_width, int32_t tex_height,
			      uint32_t mip_levels);
    vk::SampleCountFlagBits m_get_max_usable_sample_count ();
    void m_create_color_resources ();
    void m_get_key_input ();
    void m_get_cursor_input ();
    void m_bind_model (tinyobj::attrib_t* attrib, std::vector<tinyobj::shape_t>* shapes, float x_offset, float y_offset, float z_offset);
  };
} // namespace zelus
