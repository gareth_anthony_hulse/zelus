/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "vertex.hpp"

namespace zelus
{
  vk::VertexInputBindingDescription
  vertex::get_binding_description ()
  {
    vk::VertexInputBindingDescription binding_description = vk::VertexInputBindingDescription ()
      .setBinding (0)
      .setStride (sizeof (vertex))
      .setInputRate (vk::VertexInputRate::eVertex);

    return binding_description;
  }

  std::array<vk::VertexInputAttributeDescription, 3>
  vertex::get_attribute_descriptions ()
  {
    std::array<vk::VertexInputAttributeDescription, 3> attribute_descriptions = {};
    attribute_descriptions[0].binding = 0;
    attribute_descriptions[0].location = 0;
    attribute_descriptions[0].format = vk::Format::eR32G32B32Sfloat;
    attribute_descriptions[0].offset = offsetof (vertex, pos);

    attribute_descriptions[1].binding = 0;
    attribute_descriptions[1].location = 1;
    attribute_descriptions[1].format = vk::Format::eR32G32B32Sfloat;
    attribute_descriptions[1].offset = offsetof (vertex, color);

    attribute_descriptions[2].binding = 0;
    attribute_descriptions[2].location = 2;
    attribute_descriptions[2].format = vk::Format::eR32G32B32Sfloat;
    attribute_descriptions[2].offset = offsetof (vertex, tex_coord);
  
    return attribute_descriptions;
  }
}
