/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <glm/glm.hpp>

namespace zelus
{
  struct uniform_buffer_object
  {
    alignas (16) glm::mat4 model;
    alignas (16) glm::mat4 view;
    alignas (16) glm::mat4 proj;
  };
}
