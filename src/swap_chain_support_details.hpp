/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vulkan/vulkan.hpp>
#include <vector>

namespace zelus
{
  struct swap_chain_support_details
  {
    vk::SurfaceCapabilitiesKHR capabilities;
    std::vector<vk::SurfaceFormatKHR> formats;
    std::vector<vk::PresentModeKHR> present_modes;
  };
}
