/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <vulkan/vulkan.hpp>

namespace zelus
{
  struct texture
  {
    vk::Sampler sampler;
    vk::Image image;
    vk::DeviceMemory memory;
    vk::ImageLayout layout;
    vk::ImageView view;
    uint32_t mip_levels;
  };
} // namespace zelus
