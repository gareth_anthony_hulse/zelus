/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "vertex.hpp"

namespace zelus
{
  struct model
  {
    std::vector<uint32_t> indices;
    std::vector<vertex> vertices;
  };
}
