/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#define TINYOBJLOADER_IMPLMENTATION
#define STB_IMAGE_IMPLEMENTATION

#include "render.hpp"
//#include "model.hpp"

#include <iostream>
#include <thread>
#include <chrono>
#include <limits>
#include <unordered_map>
#include <stb/stb_image.h>
#include <sstream>
#include <iostream>
#include <atomic>
#include <mutex>
#include <any>

namespace
{
  // Place variables here to prevent stack smashing.
  const std::vector<const char*>
  m_device_extensions =
    {
      VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };
  
#ifndef NDEBUG
  const std::vector<const char*>
  m_validation_layers =
    {
     "VK_LAYER_KHRONOS_validation"
    };
  
  VkDebugUtilsMessengerEXT debug_messenger;
#endif // NDEBUG
}

template<> struct
std::hash<zelus::vertex>
{
  size_t operator() (const zelus::vertex& hash_vertex) const
  {
    return ((hash<glm::vec3> () (hash_vertex.pos) ^
	     (hash<glm::vec3> () (hash_vertex.color) << 1)) >> 1) ^
      (hash<glm::vec2> () (hash_vertex.tex_coord) << 1);
  }
};

namespace zelus
{ 
  // Public:
  render::render ()
  {
    m_init_window ();
    m_init_vulkan ();
  }

  void
  render::load_model (std::string& model, std::string& texture, float x_offset, float y_offset, float z_offset)
  {
    m_image_files.push_back (texture);
  
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn, err;

    if (!tinyobj::LoadObj (&attrib, &shapes, &materials, &warn, &err, model.c_str ()))
      {
	throw std::runtime_error (warn + err);
      }
  
    m_bind_model (&attrib, &shapes, x_offset, y_offset, z_offset);
  }

  void
  render::draw ()
  {
    create_texture_image ();
    m_create_vertex_buffer ();
    m_create_index_buffer ();
    m_create_uniform_buffers ();
    m_create_descriptor_pool ();
    m_create_descriptor_sets ();
    m_create_command_buffers ();
    m_create_sync_objects ();
    // Prevent crash in the destructor.
    m_draw_called = true;
    m_main_loop ();
  }

  bool
  vertex::operator== (const vertex& test_vertex) const
  {
    return
      pos == test_vertex.pos &&
      color == test_vertex.color &&
      tex_coord == test_vertex.tex_coord;
  }

  render::~render ()
  {
    m_logical_device.waitIdle ();

    m_cleanup_swap_chain ();

    for (size_t i = 0; i < m_image_files.size (); ++i)
      {
	m_logical_device.destroySampler (m_textures[i].sampler, nullptr);
	m_logical_device.destroyImageView (m_textures[i].view, nullptr);
	m_logical_device.destroyImage (m_textures[i].image, nullptr);
	m_logical_device.freeMemory (m_textures[i].memory, nullptr);
      }
    
    m_logical_device.destroyDescriptorSetLayout (m_descriptor_set_layout, nullptr);
    m_logical_device.destroyBuffer (m_index_buffer, nullptr);
    m_logical_device.freeMemory (m_index_buffer_memory, nullptr);
    m_logical_device.destroyBuffer (m_vertex_buffer, nullptr);
    m_logical_device.freeMemory (m_vertex_buffer_memory, nullptr);

    if (m_draw_called)
      {
	for (uint8_t i = 0; i < m_max_frames_in_flight; ++i)
	  {
	    m_logical_device.destroySemaphore (m_render_finished_semaphores[i], nullptr);
	    m_logical_device.destroySemaphore (m_image_available_semaphores[i], nullptr);
	    m_logical_device.destroyFence (m_in_flight_fences[i], nullptr);
	  }
      }
  
    m_logical_device.destroyCommandPool (m_command_pool, nullptr);
    m_logical_device.destroy ();
	
#ifndef NDEBUG
    m_destroy_debug_messenger (m_instance, debug_messenger, nullptr);
#endif // NDEBUG
  
    m_instance.destroySurfaceKHR (m_surface, nullptr);
    m_instance.destroy ();

    glfwDestroyWindow (m_window);
    glfwTerminate ();
  }

  // Private:
  #ifndef NDEBUG
  VkResult
  render::m_create_debug_messenger (vk::Instance instance,
				    const VkDebugUtilsMessengerCreateInfoEXT* create_info,
				    const VkAllocationCallbacks* allocator,
				    VkDebugUtilsMessengerEXT* debug_messenger)
  {
    auto create_debug_utils_messenger_ext = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>
      (instance.getProcAddr ("vkCreateDebugUtilsMessengerEXT"));
    if (create_debug_utils_messenger_ext != nullptr)
      {
	return create_debug_utils_messenger_ext (instance, create_info, allocator, debug_messenger);
      }
    
    return VK_ERROR_EXTENSION_NOT_PRESENT;
  }
  
  void
  render::m_setup_debug_messenger ()
  {
    VkDebugUtilsMessengerCreateInfoEXT create_info;
    m_populate_debug_messenger_create_info (create_info);
  
    if (m_create_debug_messenger (m_instance, &create_info, nullptr, &debug_messenger) != VK_SUCCESS)
      {
	std::runtime_error ("Failed to setup debug messenger!");
      }
  }

  VKAPI_ATTR VkBool32 VKAPI_CALL
  render::m_debug_callback (VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
			    VkDebugUtilsMessageTypeFlagsEXT message_type,
			    const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
			    void* /*user_data*/)
  {
    std::cerr << "----------------------------------------------------------------------\n"
	      << vk::to_string (static_cast<vk::DebugUtilsMessageSeverityFlagBitsEXT>( message_severity)) << ": "
	      << vk::to_string (static_cast<vk::DebugUtilsMessageTypeFlagsEXT> (message_type)) << ": "  << callback_data->pMessage << "\n"
	      << "\tMessage ID: name = " << callback_data->pMessageIdName << ", number = " << callback_data->messageIdNumber << "\n";
    
    if (0 < callback_data->queueLabelCount)
      {
        std::cerr << "\tQueue Labels:\n";
        for (uint8_t i = 0; i < callback_data->queueLabelCount; i++)
	  {
	    std::cerr << "\t\t|name = " << callback_data->pQueueLabels[i].pLabelName << "\n";
	  }
      }
    if (0 < callback_data->cmdBufLabelCount)
      {
        std::cerr << "\tCommand Buffer Labels:\n";
        for (uint8_t i = 0; i < callback_data->cmdBufLabelCount; i++)
	  {
	    std::cerr << "\t\t|name = " << callback_data->pCmdBufLabels[i].pLabelName << "\n";
	  }
      }
    if (0 < callback_data->objectCount)
      {
        for (uint8_t i = 0; i < callback_data->objectCount; i++)
	  {
	    std::cerr << "\tObject " << i << ": type = " << vk::to_string (static_cast<vk::ObjectType> (callback_data->pObjects[i].objectType)) << ", handle = "
		      << callback_data->pObjects[i].objectHandle;
	    if (callback_data->pObjects[i].pObjectName)
	      {
		std::cerr << ", name = " << callback_data->pObjects[i].pObjectName;
	      }
	    std::cerr << "\n";
	  }
      }
    std::cerr << "----------------------------------------------------------------------\n";

    return VK_TRUE;
  }

  void
  render::m_destroy_debug_messenger (vk::Instance instance, VkDebugUtilsMessengerEXT debug_messenger, const VkAllocationCallbacks* allocator)
  {
    auto destroy_debug_messenger = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>
      (instance.getProcAddr ("vkDestroyDebugUtilsMessengerEXT"));
    if (destroy_debug_messenger != nullptr)
      {
	destroy_debug_messenger (instance, debug_messenger, allocator);
      }
  }
  
  bool
  render::m_check_validation_layer_support ()
  {
    uint32_t layer_count;
    vkEnumerateInstanceLayerProperties (&layer_count, nullptr);

    std::vector<VkLayerProperties> available_layers (layer_count);
    vkEnumerateInstanceLayerProperties (&layer_count, available_layers.data ());

    for (const char* layer_name : m_validation_layers)
      {
	bool layer_found = false;

	for (const auto& layer_properties : available_layers)
	  {
	    if (strcmp (layer_name, layer_properties.layerName) == 0)
	      {
		layer_found = true;
		break;
	      }
	  }

	if (!layer_found)
	  {
	    return false;
	  }
      }
    return true;
  }

  void
  render::m_populate_debug_messenger_create_info (VkDebugUtilsMessengerCreateInfoEXT& create_info)
  {
    create_info = {};
    create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    create_info.messageSeverity =
      VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
      VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
      VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    create_info.messageType =
      VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
      VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
      VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    create_info.pfnUserCallback = m_debug_callback;  
  }
#endif //NDEBUG

  void
  render::m_bind_model (tinyobj::attrib_t* attrib, std::vector<tinyobj::shape_t>* shapes, float x_offset, float y_offset, float z_offset)
  {
    std::unordered_map<vertex, uint32_t> unique_vertices = {};
    //zelus::model model;

    for (const auto& shape : *shapes)
      {
	for (const auto& index : shape.mesh.indices)
	  {
	    vertex my_vertex = {};
		
	    my_vertex.pos =
	      {
	       (attrib->vertices[3 * index.vertex_index + 0]) + x_offset,
	       (attrib->vertices[3 * index.vertex_index + 1]) + y_offset,
	       (attrib->vertices[3 * index.vertex_index + 2]) + z_offset
	      };

	    my_vertex.tex_coord =
	      {
	       (attrib->texcoords[2 * index.texcoord_index + 0]) + x_offset,
	       (1.0f - attrib->texcoords[2 * index.texcoord_index + 1]) + y_offset
	      };

	    // Changing the values alters the colours of the model.
	    my_vertex.color = {1.0f, 1.0f, 1.0f};

	    if (unique_vertices.count (my_vertex) == 0)
	      {
		unique_vertices[my_vertex] = static_cast<uint32_t> (m_vertices.size ());
		m_vertices.push_back(my_vertex);
		//model.vertices.push_back (my_vertex);
	      }

	    m_indices.push_back (unique_vertices[my_vertex]);
	      //model.indices.push_back (unique_vertices[my_vertex]);
	  }
      }
    //u_models.push_back (model);
  }

  void
  render::m_transition_image_layout (vk::Image image, vk::Format /*format*/, vk::ImageLayout old_layout, vk::ImageLayout new_layout, uint32_t mip_levels)
  {
    vk::CommandBuffer command_buffer = m_begin_single_time_commands ();

    vk::ImageMemoryBarrier barrier = vk::ImageMemoryBarrier ()
      .setOldLayout (old_layout)
      .setNewLayout (new_layout)
      .setSrcQueueFamilyIndex (VK_QUEUE_FAMILY_IGNORED)
      .setDstQueueFamilyIndex (VK_QUEUE_FAMILY_IGNORED)
      .setImage (image)
      .setSubresourceRange (vk::ImageSubresourceRange ()
			    .setAspectMask (vk::ImageAspectFlagBits::eColor)
			    .setBaseMipLevel (0)
			    .setLevelCount (mip_levels)
			    .setBaseArrayLayer (0)
			    .setLayerCount (1));

    vk::PipelineStageFlags src_stage;
    vk::PipelineStageFlags dst_stage;

    if (old_layout == vk::ImageLayout::eUndefined && new_layout == vk::ImageLayout::eTransferDstOptimal)
      {
	barrier.setDstAccessMask (vk::AccessFlagBits::eTransferWrite);

	src_stage = vk::PipelineStageFlagBits::eTopOfPipe;
	dst_stage = vk::PipelineStageFlagBits::eTransfer;
      }
    else if (old_layout == vk::ImageLayout::eTransferDstOptimal && new_layout == vk::ImageLayout::eShaderReadOnlyOptimal)
      {
	barrier.setSrcAccessMask (vk::AccessFlagBits::eTransferWrite);
	barrier.setDstAccessMask (vk::AccessFlagBits::eShaderRead);

	src_stage = vk::PipelineStageFlagBits::eTransfer;
	dst_stage = vk::PipelineStageFlagBits::eFragmentShader;
      }
    else
      {
	std::runtime_error ("Unsupported layout transition.");
      }

    command_buffer.pipelineBarrier (src_stage, dst_stage, vk::DependencyFlags (0),	       
				    0, nullptr,
				    0, nullptr,
				    1, &barrier);

    m_end_single_time_commands (command_buffer);
  }


  void
  render::m_recreate_swap_chain ()
  {
    int width = 0, height = 0;
    while (width == 0 || height == 0)
      {
	glfwGetFramebufferSize (m_window, &width, &height);
	glfwWaitEvents ();
      }
  
    m_logical_device.waitIdle ();

    m_cleanup_swap_chain ();

    m_create_swap_chain ();
    m_create_image_views ();
    m_create_render_pass ();
    m_create_graphics_pipeline ();
    m_create_color_resources ();
    m_create_depth_resources ();
    m_create_framebuffers ();
    m_create_uniform_buffers ();
    m_create_descriptor_pool ();
    m_create_descriptor_sets ();
    m_create_command_buffers ();
  }
  
  void
  render::m_init_window ()
  {
    glfwInit ();
    GLFWmonitor *monitor = glfwGetPrimaryMonitor ();
    const GLFWvidmode *mode = glfwGetVideoMode (monitor);
  
    // Set cursor to center of the screen.
    m_last_x = mode->width / 2;
    m_last_y = mode->height / 2;

    glfwWindowHint (GLFW_CLIENT_API, GLFW_NO_API);
  
    m_window = glfwCreateWindow (mode->width, mode->height, PACKAGE_NAME, monitor, nullptr);
    glfwMakeContextCurrent(m_window);

    // Mouse:
    glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    
    if (glfwRawMouseMotionSupported())
      {
	glfwSetInputMode(m_window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
      }

    glfwSetWindowUserPointer (m_window, this);
    glfwSetInputMode (m_window, GLFW_STICKY_KEYS, GLFW_TRUE);
  }

  std::vector<const char*>
  render::m_get_required_instance_exts ()
  {
    uint32_t glfw_extension_count = 0;
    const char** glfw_extensions;
    glfw_extensions = glfwGetRequiredInstanceExtensions (&glfw_extension_count);

    std::vector<const char*> instance_extensions (glfw_extensions, glfw_extensions + glfw_extension_count);

#ifndef NDEBUG
    instance_extensions.push_back (VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif // NDEBUG

    return instance_extensions;
  }

  vk::ShaderModule
  render::m_create_shader_module (const std::vector<char>& code)
  {
    vk::ShaderModuleCreateInfo create_info = vk::ShaderModuleCreateInfo ()
      .setCodeSize (code.size ())
      .setPCode (reinterpret_cast<const uint32_t*> (code.data ()));

    vk::ShaderModule shader_module;
    if (m_logical_device.createShaderModule (&create_info, nullptr, &shader_module) != vk::Result::eSuccess)
      {
	std::runtime_error ("Failed to create shader module.");
      }

    return shader_module;
  }

  void
  render::m_create_index_buffer ()
  {
    vk::DeviceSize buffer_size = sizeof (m_indices[0]) * m_indices.size ();

    vk::Buffer staging_buffer;
    vk::DeviceMemory staging_buffer_memory;
    m_create_buffer (buffer_size, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, staging_buffer, staging_buffer_memory);

    void* data;
    vkMapMemory (m_logical_device, staging_buffer_memory, 0, buffer_size, 0, &data);
    memcpy (data, m_indices.data (), static_cast<size_t> (buffer_size));
    m_logical_device.unmapMemory (staging_buffer_memory);

    m_create_buffer (buffer_size, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eIndexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal, m_index_buffer, m_index_buffer_memory);

    m_copy_buffer (staging_buffer, m_index_buffer, buffer_size);

    m_logical_device.destroyBuffer (staging_buffer, nullptr);
    m_logical_device.freeMemory (staging_buffer_memory, nullptr);
  }

  void
  render::m_create_logical_device ()
  {
    queue_family_indices indices = m_find_queue_families ();

    std::vector<vk::DeviceQueueCreateInfo> m_logical_device_queue_create_infos;
    std::set<uint32_t> unique_queue_families =
      {
       indices.graphics_family.value (),
       indices.present_family.value ()
      };

    float queue_priority = 1.0f;
    for (uint32_t queue_family : unique_queue_families)
      {
	m_logical_device_queue_create_infos.push_back(vk::DeviceQueueCreateInfo ()
						    .setQueueFamilyIndex (queue_family)
						    .setQueueCount (1)
						    .setPQueuePriorities (&queue_priority));
      }

    vk::PhysicalDeviceFeatures physical_device_features = vk::PhysicalDeviceFeatures ()
      .setSamplerAnisotropy (VK_TRUE)
      .setSampleRateShading (VK_TRUE)
      .setAlphaToOne (VK_TRUE);

    vk::DeviceCreateInfo m_logical_device_create_info = vk::DeviceCreateInfo ()
      .setQueueCreateInfoCount (m_logical_device_queue_create_infos.size ())
      .setPQueueCreateInfos (m_logical_device_queue_create_infos.data ())
      .setPEnabledFeatures (&physical_device_features)
      .setEnabledExtensionCount (m_device_extensions.size ())
      .setPpEnabledExtensionNames (m_device_extensions.data ());

    if (m_physical_device.createDevice (&m_logical_device_create_info, nullptr, &m_logical_device)
	!= vk::Result::eSuccess)
      {
	std::runtime_error ("Failed to create logical device.");
      }

    m_logical_device.getQueue (indices.graphics_family.value (), 0, &m_graphics_queue);
    m_logical_device.getQueue (indices.present_family.value (), 0, &m_present_queue);
  }

  void
  render::m_init_vulkan ()
  {
    m_create_instance ();
#ifndef NDEBUG
    m_setup_debug_messenger ();
#endif //NDEBUG
    m_create_surface ();
    m_pick_physical_device ();
    m_create_logical_device ();
    m_create_swap_chain ();
    m_create_image_views ();
    m_create_render_pass ();
    m_create_descriptor_set_layout ();
    m_create_graphics_pipeline ();
    m_create_color_resources ();
    m_create_depth_resources ();
    m_create_framebuffers ();
    m_create_command_pool ();
  }

  std::vector<char>
  render::m_read_file (const std::string& filename)
  {
    std::ifstream file (filename, std::ios::ate | std::ios::binary);

    if (!file.is_open ())
      {
	throw std::runtime_error ("Failed to open file.");
      }

    size_t file_size = static_cast<size_t> (file.tellg ());
    std::vector<char> buffer (file_size);

    file.seekg (0);
    file.read (buffer.data (), file_size);
    file.close ();

    return buffer;
  }

  swap_chain_support_details
  render::m_query_swap_chain_support (vk::PhysicalDevice physical_device)
  {
    swap_chain_support_details details;

    static_cast<std::any> (physical_device.getSurfaceCapabilitiesKHR (m_surface, &details.capabilities));

    uint32_t format_count;
    static_cast<std::any> (physical_device.getSurfaceFormatsKHR (m_surface, &format_count, details.formats.data ()));

    if (format_count != 0)
      {
	details.formats.resize (format_count);
	static_cast<std::any> (physical_device.getSurfaceFormatsKHR (m_surface, &format_count, details.formats.data ()));
      }

    uint32_t present_mode_count;
    static_cast<std::any> (physical_device.getSurfacePresentModesKHR (m_surface, &present_mode_count, details.present_modes.data ()));

    if (present_mode_count != 0)
      {
	details.present_modes.resize (present_mode_count);
        static_cast<std::any> (physical_device.getSurfacePresentModesKHR (m_surface, &present_mode_count, details.present_modes.data ()));
      }

    return details;
  }

  void
  render::m_pick_physical_device ()
  {
    m_physical_device = m_instance.enumeratePhysicalDevices ().front ();

    if (!m_physical_device)
      {
	throw std::runtime_error ("Failed to find suitable physical device.");
      }
    m_msaa_samples = m_get_max_usable_sample_count ();
  }

  void
  render::m_create_instance ()
  {
#ifndef NDEBUG
    if (!m_check_validation_layer_support ())
      {
	throw std::runtime_error ("Validition layers not available!");
      }

    vk::DebugUtilsMessengerCreateInfoEXT debug_create_info;
    m_populate_debug_messenger_create_info (debug_create_info);
#endif // NDEBUG

    std::stringstream package_version_string;
    package_version_string << PACKAGE_VERSION;
    uint32_t package_version_int;
    package_version_string >> package_version_int;

    vk::ApplicationInfo app_info = vk::ApplicationInfo ()
      .setPEngineName (PACKAGE_NAME)
      .setEngineVersion (package_version_int)
      .setApiVersion (vk::enumerateInstanceVersion ());
  
    auto instance_extensions = m_get_required_instance_exts ();
  
    vk::InstanceCreateInfo create_info = vk::InstanceCreateInfo ()
      .setPApplicationInfo (&app_info)
      .setEnabledExtensionCount (instance_extensions.size ())
      .setPpEnabledExtensionNames (instance_extensions.data ())
#ifndef NDEBUG
      .setEnabledLayerCount (static_cast<uint32_t> (m_validation_layers.size ()))
      .setPpEnabledLayerNames (m_validation_layers.data ())
      .setPNext (static_cast<vk::DebugUtilsMessengerCreateInfoEXT*> (&debug_create_info))
#else
      .setEnabledLayerCount (0)
      .setPNext (nullptr)
#endif // NDEBUG
      ;
  
    if (vk::createInstance (&create_info, nullptr, &m_instance) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to create instance!");
      }
  }

  void
  render::m_create_render_pass ()
  {
    vk::AttachmentDescription color_attachment = vk::AttachmentDescription ()
      .setFormat (m_swap_chain_image_format)
      .setSamples (m_msaa_samples)
      .setLoadOp (vk::AttachmentLoadOp::eClear)
      .setStoreOp (vk::AttachmentStoreOp::eStore)
      .setStencilLoadOp (vk::AttachmentLoadOp::eDontCare)
      .setStencilStoreOp (vk::AttachmentStoreOp::eDontCare)
      .setInitialLayout (vk::ImageLayout::eUndefined)
      .setFinalLayout (vk::ImageLayout::eColorAttachmentOptimal);

    vk::AttachmentDescription depth_attachment = vk::AttachmentDescription ()
      .setFormat (m_find_depth_format ())
      .setSamples (m_msaa_samples)
      .setLoadOp (vk::AttachmentLoadOp::eClear)
      .setStoreOp (vk::AttachmentStoreOp::eDontCare)
      .setStencilLoadOp (vk::AttachmentLoadOp::eDontCare)
      .setStencilStoreOp (vk::AttachmentStoreOp::eDontCare)
      .setInitialLayout (vk::ImageLayout::eUndefined)
      .setFinalLayout (vk::ImageLayout::eDepthStencilAttachmentOptimal);

    vk::AttachmentDescription color_attachment_resolve = vk::AttachmentDescription ()
      .setFormat (m_swap_chain_image_format)
      .setSamples (vk::SampleCountFlagBits::e1)
      .setLoadOp (vk::AttachmentLoadOp::eDontCare)
      .setStoreOp (vk::AttachmentStoreOp::eStore)
      .setStencilLoadOp (vk::AttachmentLoadOp::eDontCare)
      .setStencilStoreOp (vk::AttachmentStoreOp::eDontCare)
      .setInitialLayout (vk::ImageLayout::eUndefined)
      .setFinalLayout (vk::ImageLayout::ePresentSrcKHR);

    vk::AttachmentReference color_attachment_ref = vk::AttachmentReference ()
      .setAttachment (0)
      .setLayout (vk::ImageLayout::eColorAttachmentOptimal);

    vk::AttachmentReference depth_attachment_ref = vk::AttachmentReference ()
      .setAttachment (1)
      .setLayout (vk::ImageLayout::eDepthStencilAttachmentOptimal);

    vk::AttachmentReference color_attachment_resolve_ref = vk::AttachmentReference ()
      .setAttachment (2)
      .setLayout (vk::ImageLayout::eColorAttachmentOptimal);

    vk::SubpassDescription subpass = vk::SubpassDescription ()
      .setPipelineBindPoint (vk::PipelineBindPoint::eGraphics)
      .setColorAttachmentCount (1)
      .setPColorAttachments (&color_attachment_ref)
      .setPDepthStencilAttachment (&depth_attachment_ref)
      .setPResolveAttachments (&color_attachment_resolve_ref);

    vk::SubpassDependency dependency = vk::SubpassDependency ()
      .setSrcSubpass (VK_SUBPASS_EXTERNAL)
      .setDstSubpass (0)
      .setSrcStageMask (vk::PipelineStageFlagBits::eColorAttachmentOutput)
      .setSrcAccessMask (vk::AccessFlagBits::eMemoryRead)
      .setDstStageMask (vk::PipelineStageFlagBits::eColorAttachmentOutput)
      .setDstAccessMask (vk::AccessFlagBits::eColorAttachmentRead |
			 vk::AccessFlagBits::eColorAttachmentWrite);

    std::array<vk::AttachmentDescription, 3> attachments = {color_attachment, depth_attachment, color_attachment_resolve};
    vk::RenderPassCreateInfo render_pass_info = vk::RenderPassCreateInfo ()
      .setAttachmentCount (static_cast<uint32_t> (attachments.size ()))
      .setPAttachments (attachments.data ())
      .setSubpassCount (1)
      .setPSubpasses (&subpass)
      .setDependencyCount (1)
      .setPDependencies (&dependency);

    if (m_logical_device.createRenderPass (&render_pass_info, nullptr, &m_render_pass) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to create render pass.");
      }
  }

  void
  render::m_create_image_views ()
  {
    m_swap_chain_image_views.resize (m_swap_chain_images.size ());

    for (size_t i = 0; i < m_swap_chain_images.size (); ++i)
      {
	m_swap_chain_image_views[i] = m_create_image_view (m_swap_chain_images[i],
							   m_swap_chain_image_format,
							   vk::ImageAspectFlagBits::eColor,
							   1);
      }
  }

  vk::ImageView
  render::m_create_image_view (vk::Image image, vk::Format format, vk::ImageAspectFlags aspect_flags, uint32_t mip_levels)
  {
    vk::ImageViewCreateInfo view_info = vk::ImageViewCreateInfo ()
      .setImage (image)
      .setViewType (vk::ImageViewType::e2D)
      .setFormat (format)
      .setSubresourceRange (vk::ImageSubresourceRange ()
			    .setAspectMask (aspect_flags)
			    .setBaseMipLevel (0)
			    .setLevelCount (mip_levels)
			    .setBaseArrayLayer (0)
			    .setLayerCount (1));

    vk::ImageView image_view;
    if (m_logical_device.createImageView (&view_info, nullptr, &image_view) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to create texture image view.");
      }

    return image_view;
  }

  void
  render::m_create_graphics_pipeline ()
  {
    auto vert_shader_code = m_read_file (PKGDATADIR "/shader/triangle.vert.spv");
    auto frag_shader_code = m_read_file (PKGDATADIR "/shader/triangle.frag.spv");

    vk::ShaderModule vert_shader_module = m_create_shader_module (vert_shader_code);
    vk::ShaderModule frag_shader_module = m_create_shader_module (frag_shader_code);

    vk::PipelineShaderStageCreateInfo vert_shader_stage_info = vk::PipelineShaderStageCreateInfo ()
      .setStage (vk::ShaderStageFlagBits::eVertex)
      .setModule (vert_shader_module)
      .setPName ("main");

    vk::PipelineShaderStageCreateInfo frag_shader_stage_info = vk::PipelineShaderStageCreateInfo ()
      .setStage (vk::ShaderStageFlagBits::eFragment)
      .setModule (frag_shader_module)
      .setPName ("main");

    vk::PipelineShaderStageCreateInfo shader_stages[] =
      {
       vert_shader_stage_info,
       frag_shader_stage_info
      };

    auto binding_description = vertex::get_binding_description ();
    auto attribute_descriptions = vertex::get_attribute_descriptions ();
  
    vk::PipelineVertexInputStateCreateInfo vertex_input_info = vk::PipelineVertexInputStateCreateInfo ()
      .setVertexBindingDescriptionCount (1)
      .setVertexAttributeDescriptionCount (static_cast<uint32_t> (attribute_descriptions.size ()))
      .setPVertexBindingDescriptions (&binding_description)
      .setPVertexAttributeDescriptions (attribute_descriptions.data ());

    vk::PipelineInputAssemblyStateCreateInfo input_assembly = vk::PipelineInputAssemblyStateCreateInfo ()
      .setTopology (vk::PrimitiveTopology::eTriangleList)
      .setPrimitiveRestartEnable (VK_FALSE);

    vk::Viewport viewport = vk::Viewport ()
      .setX (0.0f)
      .setY (0.0f)
      .setWidth (static_cast<float> (m_swap_chain_extent.width))
      .setHeight (static_cast<float> (m_swap_chain_extent.height))
      .setMinDepth (0.0f)
      .setMaxDepth (1.0f);

    vk::Rect2D scissor = vk::Rect2D ()
      .setOffset({0, 0})
      .setExtent (m_swap_chain_extent);

    vk::PipelineViewportStateCreateInfo viewport_state = vk::PipelineViewportStateCreateInfo ()
      .setViewportCount (1)
      .setPViewports (&viewport)
      .setScissorCount (1)
      .setPScissors (&scissor);

    vk::PipelineRasterizationStateCreateInfo rasterizer = vk::PipelineRasterizationStateCreateInfo ()
      .setDepthClampEnable (VK_FALSE)
      .setRasterizerDiscardEnable (VK_FALSE)
      .setPolygonMode (vk::PolygonMode::eFill)
      .setLineWidth (1.0f)
      .setCullMode (vk::CullModeFlagBits::eBack)
      .setFrontFace (vk::FrontFace::eCounterClockwise)
      .setDepthBiasEnable (VK_FALSE);

    vk::PipelineMultisampleStateCreateInfo multisampling = vk::PipelineMultisampleStateCreateInfo ()
      .setSampleShadingEnable (VK_TRUE)
      .setRasterizationSamples (m_msaa_samples)
      .setMinSampleShading (1.0f);

    vk::PipelineDepthStencilStateCreateInfo depth_stencil = vk::PipelineDepthStencilStateCreateInfo ()
      .setDepthTestEnable (VK_TRUE)
      .setDepthWriteEnable (VK_TRUE)
      .setDepthCompareOp (vk::CompareOp::eLess)
      .setDepthBoundsTestEnable ( VK_FALSE);

    vk::PipelineColorBlendAttachmentState color_blend_attachment = vk::PipelineColorBlendAttachmentState ()
      .setColorWriteMask
      (vk::ColorComponentFlagBits::eR |
       vk::ColorComponentFlagBits::eG |
       vk::ColorComponentFlagBits::eB |
       vk::ColorComponentFlagBits::eA)
      .setBlendEnable (VK_FALSE);

    vk::PipelineColorBlendStateCreateInfo color_blending = vk::PipelineColorBlendStateCreateInfo ()
      .setLogicOpEnable (VK_FALSE)
      .setAttachmentCount(1)
      .setPAttachments (&color_blend_attachment);

    vk::PipelineLayoutCreateInfo pipeline_layout_info = vk::PipelineLayoutCreateInfo ()
      .setSetLayoutCount (1)
      .setPSetLayouts (&m_descriptor_set_layout);

    if (m_logical_device.createPipelineLayout (&pipeline_layout_info, nullptr, &m_pipeline_layout)
	!= vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to create pipeline layout.");
      }

    vk::GraphicsPipelineCreateInfo pipeline_info = vk::GraphicsPipelineCreateInfo ()
      .setStageCount (2)
      .setPStages (shader_stages)
      .setPVertexInputState  (&vertex_input_info)
      .setPInputAssemblyState (&input_assembly)
      .setPViewportState (&viewport_state)
      .setPRasterizationState (&rasterizer)
      .setPMultisampleState (&multisampling)
      .setPDepthStencilState (&depth_stencil)
      .setPColorBlendState (&color_blending)
      .setLayout (m_pipeline_layout)
      .setRenderPass (m_render_pass)
      .setSubpass (0);

    if (m_logical_device.createGraphicsPipelines (nullptr, 1, &pipeline_info, nullptr, &m_graphics_pipeline) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to create graphics pipeline.");
      }
  
    m_logical_device.destroyShaderModule (frag_shader_module, nullptr);
    m_logical_device.destroyShaderModule (vert_shader_module, nullptr);
  }

  void
  render::m_create_framebuffers ()
  {
    m_swap_chain_framebuffers.resize (m_swap_chain_image_views.size ());

    for (size_t i = 0; i < m_swap_chain_image_views.size (); ++i)
      {
	std::array<vk::ImageView, 3> attachments =
	  {
	   m_color_image_view,
	   m_depth_image_view,
	   m_swap_chain_image_views[i]
	  };

	vk::FramebufferCreateInfo framebuffer_info = vk::FramebufferCreateInfo ()
	  .setRenderPass (m_render_pass)
	  .setAttachmentCount (static_cast<uint32_t> (attachments.size ()))
	  .setPAttachments (attachments.data ())
	  .setWidth (m_swap_chain_extent.width)
	  .setHeight (m_swap_chain_extent.height)
	  .setLayers (1);

	if (m_logical_device.createFramebuffer (&framebuffer_info, nullptr,
						&m_swap_chain_framebuffers[i]) != vk::Result::eSuccess)
	  {
	    throw std::runtime_error ("Failed to create framebuffer.");
	  }
      };
  }

  void
  render::m_create_descriptor_sets ()
  {
    std::vector<vk::DescriptorSetLayout> layouts (m_swap_chain_images.size (), m_descriptor_set_layout);
    vk::DescriptorSetAllocateInfo alloc_info = vk::DescriptorSetAllocateInfo ()
      .setDescriptorPool (m_descriptor_pool)
      .setDescriptorSetCount (static_cast<uint32_t> (m_swap_chain_images.size ()))
      .setPSetLayouts (layouts.data ());

    m_descriptor_sets.resize (m_swap_chain_images.size ());
    if (m_logical_device.allocateDescriptorSets (&alloc_info, m_descriptor_sets.data ()) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to allocate descriptor sets.");
      }

    for (size_t i = 0; i < m_swap_chain_images.size (); ++i)
      {
	vk::DescriptorBufferInfo buffer_info = vk::DescriptorBufferInfo ()
	  .setBuffer (m_uniform_buffers[i])
	  .setOffset (0)
	  .setRange (sizeof (uniform_buffer_object));

	  
	vk::DescriptorImageInfo image_info = vk::DescriptorImageInfo ()
	  .setImageLayout (vk::ImageLayout::eShaderReadOnlyOptimal)
	  .setImageView (m_textures[0].view)
	  .setSampler (m_textures[0].sampler);
	
	std::array<vk::WriteDescriptorSet, 2> descriptor_writes = {};
	descriptor_writes[0].setDstSet (m_descriptor_sets[i]);
	descriptor_writes[0].setDstBinding (0);
	descriptor_writes[0].setDstArrayElement (0);
	descriptor_writes[0].setDescriptorType (vk::DescriptorType::eUniformBuffer);
	descriptor_writes[0].setDescriptorCount (1);
	descriptor_writes[0].setPBufferInfo (&buffer_info);
      
	descriptor_writes[1].setDstSet (m_descriptor_sets[i]);
	descriptor_writes[1].setDstBinding (1);
	descriptor_writes[1].setDstArrayElement (0);
	descriptor_writes[1].setDescriptorType (vk::DescriptorType::eCombinedImageSampler);
	descriptor_writes[1].setDescriptorCount (1);
	descriptor_writes[1].setPImageInfo (&image_info);
	    
	m_logical_device.updateDescriptorSets (static_cast<uint32_t> (descriptor_writes.size ()), descriptor_writes.data (), 0, nullptr);
      }
  }


  void
  render::m_create_descriptor_set_layout ()
  {
    vk::DescriptorSetLayoutBinding ubo_layout_binding = vk::DescriptorSetLayoutBinding ()
      .setBinding (0)
      .setDescriptorCount (1)
      .setDescriptorType (vk::DescriptorType::eUniformBuffer)
      .setPImmutableSamplers (nullptr)
      .setStageFlags (vk::ShaderStageFlagBits::eVertex);

    vk::DescriptorSetLayoutBinding sampler_layout_binding = vk::DescriptorSetLayoutBinding ()
      .setBinding (1)
      .setDescriptorCount (1)
      .setDescriptorType (vk::DescriptorType::eCombinedImageSampler)
      .setPImmutableSamplers (nullptr)
      .setStageFlags (vk::ShaderStageFlagBits::eFragment);

    std::array<vk::DescriptorSetLayoutBinding, 2> bindings = {ubo_layout_binding, sampler_layout_binding};

    vk::DescriptorSetLayoutCreateInfo layout_info = vk::DescriptorSetLayoutCreateInfo ()
      .setBindingCount (static_cast<uint32_t> (bindings.size ()))
      .setPBindings (bindings.data ());

    if (m_logical_device.createDescriptorSetLayout (&layout_info, nullptr, &m_descriptor_set_layout) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to create descriptor set layout.");
      }
  }

  void
  render::m_create_descriptor_pool ()
  {
    std::array<vk::DescriptorPoolSize, 2> pool_sizes = {};
    pool_sizes[0].type = vk::DescriptorType::eUniformBuffer;
    pool_sizes[0].descriptorCount = static_cast<uint32_t> (m_swap_chain_images.size ());
    pool_sizes[1].type = vk::DescriptorType::eCombinedImageSampler;
    pool_sizes[1].descriptorCount = static_cast<uint32_t> (m_swap_chain_images.size ());

    vk::DescriptorPoolCreateInfo pool_info = vk::DescriptorPoolCreateInfo ()
      .setPoolSizeCount (static_cast<uint32_t> (pool_sizes.size ()))
      .setPPoolSizes (pool_sizes.data ())
      .setMaxSets (static_cast<uint32_t> (m_swap_chain_images.size ()));

    if (m_logical_device.createDescriptorPool (&pool_info, nullptr, &m_descriptor_pool) != vk::Result::eSuccess)
      {
	throw std::runtime_error("Failed to create descriptor pool.");
      }
  }

  void
  render::m_create_depth_resources ()
  {
    vk::Format depth_format = m_find_depth_format ();
    m_create_image (m_swap_chain_extent.width,
		    m_swap_chain_extent.height,
		    1,
		    m_msaa_samples,
		    depth_format,
		    vk::ImageTiling::eOptimal,
		    vk::ImageUsageFlagBits::eDepthStencilAttachment,
		    vk::MemoryPropertyFlagBits::eDeviceLocal,
		    m_depth_image,
		    m_depth_image_memory);
  
    m_depth_image_view = m_create_image_view (m_depth_image, depth_format, vk::ImageAspectFlagBits::eDepth, 1);
  }

  void
  render::m_create_command_pool ()
  {
    queue_family_indices my_queue_family_indices = m_find_queue_families ();

    vk::CommandPoolCreateInfo pool_info = vk::CommandPoolCreateInfo ()
      .setQueueFamilyIndex (my_queue_family_indices.graphics_family.value ());

    if (m_logical_device.createCommandPool (&pool_info, nullptr, &m_command_pool) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to create command pool.");
      }
  }

  void
  render::m_create_command_buffers ()
  {
    m_command_buffers.resize (m_swap_chain_framebuffers.size ());

    vk::CommandBufferAllocateInfo alloc_info = vk::CommandBufferAllocateInfo ()
      .setCommandPool (m_command_pool)
      .setLevel (vk::CommandBufferLevel::ePrimary)
      .setCommandBufferCount (static_cast<uint32_t> (m_command_buffers.size ()));

    if (m_logical_device.allocateCommandBuffers (&alloc_info, m_command_buffers.data ()) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to allocate command buffers.");
      }

    for (size_t i = 0; i < m_command_buffers.size (); ++i)
      {
	vk::CommandBufferBeginInfo begin_info = vk::CommandBufferBeginInfo ();

	if (m_command_buffers[i].begin (&begin_info) != vk::Result::eSuccess)
	  {
	    throw std::runtime_error ("Failed to begin recording command buffer.");
	  }

	vk::RenderPassBeginInfo render_pass_info = vk::RenderPassBeginInfo ()
	  .setRenderPass (m_render_pass)
	  .setFramebuffer (m_swap_chain_framebuffers[i])
	  .setRenderArea (vk::Rect2D ()
			  .setOffset ({0, 0})
			  .setExtent (m_swap_chain_extent));

      
	std::array <vk::ClearValue, 2> clear_values = {};
	clear_values[0].setColor (vk::ClearColorValue ().setFloat32 ({0.0f, 0.0f, 0.0f, 1.0f}));
	clear_values[1].setDepthStencil (vk::ClearDepthStencilValue(1.0f, 0));

	render_pass_info
	  .setClearValueCount (static_cast<uint32_t> (clear_values.size ()))
	  .setPClearValues (clear_values.data ());

	m_command_buffers[i].beginRenderPass (&render_pass_info, vk::SubpassContents::eInline);
	m_command_buffers[i].bindPipeline (vk::PipelineBindPoint::eGraphics, m_graphics_pipeline);

	vk::Buffer vertex_buffers[] = {m_vertex_buffer};
	vk::DeviceSize offsets[] = {0};
	m_command_buffers[i].bindVertexBuffers (0, 1, vertex_buffers, offsets);
	m_command_buffers[i].bindIndexBuffer (m_index_buffer, 0, vk::IndexType::eUint32);
	m_command_buffers[i].bindDescriptorSets (vk::PipelineBindPoint::eGraphics, m_pipeline_layout, 0, 1, &m_descriptor_sets[i], 0, nullptr);
	m_command_buffers[i].drawIndexed (static_cast<uint32_t> (m_indices.size ()), 1, 0, 0, 0);
	m_command_buffers[i].endRenderPass ();
	m_command_buffers[i].end ();
      }
  }

  void
  render::m_cleanup_swap_chain ()
  {
    m_logical_device.destroyImageView (m_depth_image_view, nullptr);
    m_logical_device.destroyImage (m_depth_image, nullptr);
    m_logical_device.freeMemory (m_depth_image_memory, nullptr);
    m_logical_device.destroyImageView (m_color_image_view, nullptr);
    m_logical_device.destroyImage (m_color_image, nullptr);
    m_logical_device.freeMemory (m_color_image_memory, nullptr);
  
    for (auto framebuffer : m_swap_chain_framebuffers)
      {
	m_logical_device.destroyFramebuffer (framebuffer, nullptr);
      }
  
    m_logical_device.destroyPipeline (m_graphics_pipeline, nullptr);
    m_logical_device.destroyPipelineLayout (m_pipeline_layout, nullptr);
    m_logical_device.destroyRenderPass (m_render_pass, nullptr);
  
    for (auto image_view : m_swap_chain_image_views)
      {
	m_logical_device.destroyImageView (image_view, nullptr);
      }
  
    m_logical_device.destroySwapchainKHR (m_swap_chain, nullptr);

    if (m_draw_called)
      {
	for (size_t i = 0; i < m_swap_chain_images.size (); ++i)
	  {
	    m_logical_device.destroyBuffer (m_uniform_buffers[i], nullptr);
	    m_logical_device.freeMemory (m_uniform_buffers_memory[i], nullptr);
	  }
      }

    m_logical_device.destroyDescriptorPool (m_descriptor_pool, nullptr);
  }

  vk::SurfaceFormatKHR
  render::m_choose_swap_surface_format (const std::vector<vk::SurfaceFormatKHR>& available_formats)
  {
    for (const auto& available_format : available_formats)
      {
	if (available_format.format == vk::Format::eB8G8R8A8Unorm &&
	    available_format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
	  {
	    return available_format;
	  }
      }
    return available_formats[0];
  }

  vk::PresentModeKHR
  render::m_choose_swap_present_mode (const std::vector<vk::PresentModeKHR>& available_present_modes)
  {
    vk::PresentModeKHR set_present_mode = vk::PresentModeKHR::eFifo;
    
    // The last present mode listed will be choosen if available.
    for (const auto& available_present_mode : available_present_modes)
      {
	if (available_present_mode == vk::PresentModeKHR::eMailbox)
	  {
	    set_present_mode = available_present_mode;
	  }
	
	if (available_present_mode == vk::PresentModeKHR::eImmediate)
	  {
	    set_present_mode = available_present_mode;
	  }
      }
    
    return set_present_mode;
  }


  vk::Extent2D
  render::m_choose_swap_extent (const vk::SurfaceCapabilitiesKHR& capabilities)
  {
    if (capabilities.currentExtent.width != UINT32_MAX)
      {
	return capabilities.currentExtent;
      }

    int new_width, new_height;
    glfwGetFramebufferSize (m_window, &new_width, &new_height);
    vk::Extent2D actual_extent =
      {
       static_cast<uint32_t> (new_width),
       static_cast<uint32_t> (new_height)
      };
  
    actual_extent.width = std::max (capabilities.minImageExtent.width,
				    std::min (capabilities.maxImageExtent.width,
					      actual_extent.width));
    actual_extent.height = std::max (capabilities.minImageExtent.width,
				     std::min (capabilities.maxImageExtent.height,
					       actual_extent.height));
    return actual_extent;
  }


  vk::CommandBuffer
  render::m_begin_single_time_commands ()
  {
    vk::CommandBufferAllocateInfo alloc_info = vk::CommandBufferAllocateInfo ()
      .setLevel (vk::CommandBufferLevel::ePrimary)
      .setCommandPool (m_command_pool)
      .setCommandBufferCount (1);
  
    vk::CommandBuffer command_buffer;
    static_cast<std::any> (m_logical_device.allocateCommandBuffers (&alloc_info, &command_buffer));

    vk::CommandBufferBeginInfo begin_info = vk::CommandBufferBeginInfo ()
      .setFlags (vk::CommandBufferUsageFlagBits::eOneTimeSubmit);

    static_cast<std::any> (command_buffer.begin (&begin_info));

    return command_buffer;
  }

  void
  render::m_create_sync_objects ()
  {
    m_image_available_semaphores.resize (m_max_frames_in_flight);
    m_render_finished_semaphores.resize (m_max_frames_in_flight);
    m_in_flight_fences.resize (m_max_frames_in_flight);
    m_images_in_flight.resize (m_swap_chain_images.size (), nullptr);
  
    vk::SemaphoreCreateInfo semaphore_info = vk::SemaphoreCreateInfo ();

    vk::FenceCreateInfo fence_info = vk::FenceCreateInfo ()
      .setFlags (vk::FenceCreateFlagBits::eSignaled);

    for (size_t i = 0; i < m_max_frames_in_flight; ++i)
      {
	if (m_logical_device.createSemaphore (&semaphore_info,
					      nullptr,
					      &m_image_available_semaphores[i]) != vk::Result::eSuccess ||
	    m_logical_device.createSemaphore (&semaphore_info,
					      nullptr,
					      &m_render_finished_semaphores[i]) != vk::Result::eSuccess ||
	    m_logical_device.createFence (&fence_info,
					  nullptr,
					  &m_in_flight_fences[i]) != vk::Result::eSuccess)
	  {
	    throw std::runtime_error ("Failed to create synchronization objects for a frame.");
	  }
      }
  }

  void
  render::m_create_swap_chain ()
  {
    swap_chain_support_details swap_chain_support = m_query_swap_chain_support (m_physical_device);
    vk::SurfaceFormatKHR surface_format = m_choose_swap_surface_format (swap_chain_support.formats);
    vk::PresentModeKHR present_mode = m_choose_swap_present_mode (swap_chain_support.present_modes);
    vk::Extent2D extent = m_choose_swap_extent (swap_chain_support.capabilities);

    uint32_t image_count = swap_chain_support.capabilities.minImageCount + 1;
    if (swap_chain_support.capabilities.maxImageCount > 0 && image_count > swap_chain_support.capabilities.maxImageCount)
      {
	image_count = swap_chain_support.capabilities.maxImageCount;
      }

    vk::SwapchainCreateInfoKHR create_info = vk::SwapchainCreateInfoKHR ()
      .setSurface (m_surface)
      .setMinImageCount (image_count)
      .setImageFormat (surface_format.format)
      .setImageColorSpace (surface_format.colorSpace)
      .setImageExtent (extent)
      .setImageArrayLayers (1)
      .setImageUsage (vk::ImageUsageFlagBits::eColorAttachment)
      .setPreTransform (swap_chain_support.capabilities.currentTransform)
      .setCompositeAlpha (vk::CompositeAlphaFlagBitsKHR::eOpaque)
      .setPresentMode (present_mode)
      .setClipped (VK_TRUE)
      .setOldSwapchain (m_swap_chain);

    queue_family_indices indices = m_find_queue_families ();
    uint32_t my_queue_family_indices[] =
      {
       indices.graphics_family.value (),
       indices.present_family.value ()
      };

    if (indices.graphics_family != indices.present_family)
      {
	create_info
	  .setImageSharingMode (vk::SharingMode::eConcurrent)
	  .setQueueFamilyIndexCount (2) 
	  .setPQueueFamilyIndices (my_queue_family_indices);
      }
    else
      {
	create_info.setImageSharingMode (vk::SharingMode::eExclusive);
      }

    if (m_logical_device.createSwapchainKHR (&create_info, nullptr, &m_swap_chain) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to create swap chain!");
      }

    vkGetSwapchainImagesKHR (m_logical_device, m_swap_chain, &image_count, nullptr);
    m_swap_chain_images.resize (image_count);
    static_cast<std::any> (m_logical_device.getSwapchainImagesKHR (m_swap_chain, &image_count, m_swap_chain_images.data ()));

    m_swap_chain_image_format = surface_format.format;
    m_swap_chain_extent = extent;
  }

  void
  render::m_create_surface ()
  {
    if (glfwCreateWindowSurface (m_instance, m_window, nullptr, &m_surface) != VK_SUCCESS)
      {
	throw std::runtime_error ("Failed to create window surface!");
      }
  }

  void
  render::m_get_key_input ()
  {
    float velocity = 2.0f * m_delta_time;
  
    if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
      {
	glfwSetWindowShouldClose(m_window, true);
      }

    if (glfwGetKey(m_window, GLFW_KEY_W) != GLFW_RELEASE)
      {
	m_camera_pos += velocity * m_camera_front;
      }
    if (glfwGetKey(m_window, GLFW_KEY_A) != GLFW_RELEASE)
      {
	m_camera_pos -= glm::normalize(glm::cross(m_camera_front, m_camera_up)) * velocity;
      }
    if (glfwGetKey(m_window, GLFW_KEY_S) != GLFW_RELEASE)
      {
	m_camera_pos -= velocity * m_camera_front;
      }
    if (glfwGetKey(m_window, GLFW_KEY_D) != GLFW_RELEASE)
      {
	m_camera_pos += glm::normalize(glm::cross(m_camera_front, m_camera_up)) * velocity;
      }
    if (glfwGetKey(m_window, GLFW_KEY_LEFT_CONTROL) != GLFW_RELEASE)
      {
	m_camera_pos.z -= m_camera_up.z * velocity;
      }
    if (glfwGetKey(m_window, GLFW_KEY_SPACE) != GLFW_RELEASE)
      {
	m_camera_pos.z += m_camera_up.z * velocity;
      }

    // Gamepad:
    /*
      glfwGetGamepadState(GLFW_JOYSTICK_1, &state);
      m_camera_pos += glm::normalize(glm::cross(m_camera_front, m_camera_up)) *
      (velocity * state.axes[GLFW_GAMEPAD_AXIS_LEFT_X]);
      m_camera_pos -= (velocity * state.axes[GLFW_GAMEPAD_AXIS_LEFT_Y]) * m_camera_front;
    */
  }

  void
  render::m_get_cursor_input ()
  {
    // Cursor:
    {
      double x_pos, y_pos;
      glfwGetCursorPos (m_window, &x_pos, &y_pos);

      if (m_first_cursor)
	{
	  m_last_x = x_pos;
	  m_last_y = y_pos;
	  m_first_cursor = false;
	}

      double x_offset = x_pos - m_last_x;
      double y_offset = m_last_y - y_pos;
      m_last_x = x_pos;
      m_last_y = y_pos;

      float sensitivity = 0.1f;
      x_offset *= -sensitivity;
      y_offset *= sensitivity;

      m_yaw += x_offset;
      m_pitch += y_offset;
    }
  
    // Gamepad:
    /*
      glfwGetGamepadState(GLFW_JOYSTICK_1, &state);

      double sensitivity = 2.0f;
  
      m_yaw -= state.axes[GLFW_GAMEPAD_AXIS_RIGHT_X] * m_delta_time * sensitivity;
      m_pitch -= state.axes[GLFW_GAMEPAD_AXIS_RIGHT_Y] * m_delta_time * sensitivity;
    */

    // Prevent wrapping of m_pitch.
    if (m_pitch > 89.0f)
      {
	m_pitch = 89.0f;
      }
    if (m_pitch < -89.0f)
      {
	m_pitch = -89.0f;
      }

    // Limit value of m_yaw.
    if (m_yaw < -180.0f)
      {
	m_yaw += 360.0f;
      }
    if (m_yaw > 180.0f)
      {
	m_yaw -= 360.0f;
      }
  
    glm::vec3 front;
    front.x = cos (glm::radians (m_yaw)) * cos (glm::radians (m_pitch));
    front.y = sin (glm::radians (m_yaw)) * cos (glm::radians (m_pitch));
    front.z = sin (glm::radians (m_pitch)); 
    m_camera_front = front;
  }

  vk::Format
  render::m_find_supported_format (const std::vector<vk::Format>& candidates, vk::ImageTiling tiling,
					  vk::FormatFeatureFlags features)
  {
    for (vk::Format format : candidates)
      {
	vk::FormatProperties props;
	m_physical_device.getFormatProperties (format, &props);

	if (tiling == vk::ImageTiling::eLinear && (props.linearTilingFeatures & features) == features)
	  {
	    return format;
	  }
	else if (tiling == vk::ImageTiling::eOptimal && (props.optimalTilingFeatures & features) == features)
	  {
	    return format;
	  }
      }
    throw std::runtime_error ("Failed to find supported format.");
  }

  uint32_t
  render::m_find_memory_type (uint32_t type_filter, vk::MemoryPropertyFlags properties)
  {
    vk::PhysicalDeviceMemoryProperties mem_properties;
    m_physical_device.getMemoryProperties (&mem_properties);

    for (uint32_t i = 0; i < mem_properties.memoryTypeCount; ++i)
      {
	if ((type_filter & (1 << i)) &&
	    (mem_properties.memoryTypes[i].propertyFlags & properties) == properties)
	  {
	    return i;
	  }
      }
    throw std::runtime_error ("Failed to find suitable memory type.");
  }

  vk::Format
  zelus::render::m_find_depth_format ()
  {
    return m_find_supported_format ({vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint, vk::Format::eD24UnormS8Uint}, vk::ImageTiling::eOptimal, vk::FormatFeatureFlagBits::eDepthStencilAttachment);
  }

  void
  render::m_end_single_time_commands (vk::CommandBuffer command_buffer)
  {
    command_buffer.end ();

    vk::SubmitInfo submit_info = vk::SubmitInfo ()
      .setCommandBufferCount (1)
      .setPCommandBuffers (&command_buffer);

    static_cast<std::any> (m_graphics_queue.submit (1, &submit_info, nullptr));
    m_graphics_queue.waitIdle ();

    m_logical_device.freeCommandBuffers (m_command_pool, 1, &command_buffer);
  }

  void
  render::m_create_vertex_buffer ()
  {
    vk::DeviceSize buffer_size = sizeof (m_vertices[0]) * m_vertices.size ();

    vk::Buffer staging_buffer;
    vk::DeviceMemory staging_buffer_memory;
    m_create_buffer (buffer_size, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, staging_buffer, staging_buffer_memory);

    void* data;
    vkMapMemory (m_logical_device, staging_buffer_memory, 0, buffer_size, 0, &data);
    memcpy (data, m_vertices.data (), static_cast<size_t> (buffer_size));
    m_logical_device.unmapMemory (staging_buffer_memory);

    m_create_buffer (buffer_size, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eVertexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal, m_vertex_buffer, m_vertex_buffer_memory);

    m_copy_buffer (staging_buffer, m_vertex_buffer, buffer_size);

    m_logical_device.destroyBuffer (staging_buffer, nullptr);
    m_logical_device.freeMemory (staging_buffer_memory, nullptr);
  }


  void
  render::m_create_uniform_buffers ()
  {
    vk::DeviceSize buffer_size = sizeof (uniform_buffer_object);

    m_uniform_buffers.resize (m_swap_chain_images.size ());
    m_uniform_buffers_memory.resize (m_swap_chain_images.size ());

    for (size_t i = 0; i < m_swap_chain_images.size (); ++i)
      {
	m_create_buffer (buffer_size, vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, m_uniform_buffers[i], m_uniform_buffers_memory[i]);
      }
  }

  void
  render::create_texture_image ()
  {
    m_textures.reserve (m_image_files.size ());
    for (std::size_t i = 0; i < m_image_files.size (); ++i)
      {
	vk::Sampler sampler;
	int tex_width, tex_height, tex_channels;
  
	stbi_uc* pixels = stbi_load (m_image_files[i].c_str (), &tex_width, &tex_height,
				     &tex_channels, STBI_rgb_alpha);
	vk::DeviceSize image_size = tex_width * tex_height * 4;
	m_textures[i].mip_levels = static_cast<uint32_t> (std::floor (std::log2 (std::max (tex_width, tex_height)))) + 1;

	if (!pixels)
	  {
	    throw std::runtime_error ("Failed to load texture image.");
	  }

	vk::Buffer staging_buffer;
	vk::DeviceMemory staging_buffer_memory;
	m_create_buffer (image_size, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, staging_buffer, staging_buffer_memory);

	void* data;
	vkMapMemory (m_logical_device, staging_buffer_memory, 0, image_size, 0, &data);
	memcpy (data, pixels, static_cast<size_t> (image_size));
	m_logical_device.unmapMemory (staging_buffer_memory);

	stbi_image_free (pixels);

	m_create_image (tex_width, tex_height, m_textures[i].mip_levels, vk::SampleCountFlagBits::e1, vk::Format::eR8G8B8A8Unorm, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eTransferSrc |
			vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled, vk::MemoryPropertyFlagBits::eDeviceLocal, m_textures[i].image, m_textures[i].memory);

	m_transition_image_layout (m_textures[i].image, vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal, m_textures[i].mip_levels);
  
	m_copy_buffer_to_image (staging_buffer, m_textures[i].image, static_cast<uint32_t> (tex_width), static_cast<uint32_t> (tex_height));

	m_logical_device.destroyBuffer (staging_buffer, nullptr);
	m_logical_device.freeMemory (staging_buffer_memory, nullptr);

	m_generate_mipmaps (m_textures[i].image, vk::Format::eR8G8B8A8Unorm, tex_width, tex_height, m_textures[i].mip_levels);

	m_textures[i].view = m_create_image_view (m_textures[i].image, vk::Format::eR8G8B8A8Unorm, vk::ImageAspectFlagBits::eColor, m_textures[i].mip_levels);

	vk::SamplerCreateInfo sampler_info = vk::SamplerCreateInfo ()
	  .setMagFilter (vk::Filter::eLinear)
	  .setMinFilter (vk::Filter::eLinear)
	  .setAddressModeU (vk::SamplerAddressMode::eRepeat)
	  .setAddressModeV (vk::SamplerAddressMode::eRepeat)
	  .setAddressModeW (vk::SamplerAddressMode::eRepeat)
	  .setAnisotropyEnable (VK_TRUE)
	  .setMaxAnisotropy (16)
	  .setBorderColor (vk::BorderColor::eIntOpaqueBlack)
	  .setUnnormalizedCoordinates (VK_FALSE)
	  .setCompareEnable (VK_FALSE)
	  .setCompareOp (vk::CompareOp::eAlways)
	  .setMipmapMode (vk::SamplerMipmapMode::eLinear)
	  .setMipLodBias (0.0f)
	  .setMinLod (0.0f)
	  .setMaxLod (static_cast<float> (m_textures[i].mip_levels));

	if (m_logical_device.createSampler (&sampler_info, nullptr, &m_textures[i].sampler) != vk::Result::eSuccess)
	  {
	    throw std::runtime_error ("Failed to create texture sampler.");
	  }
      }
  }

  void
  render::m_create_image (uint32_t width, uint32_t height, uint32_t mip_levels, vk::SampleCountFlagBits num_samples, vk::Format format, vk::ImageTiling tiling, vk::ImageUsageFlags usage,
			  vk::MemoryPropertyFlags properties, vk::Image& image, vk::DeviceMemory& image_memory)
  {
    vk::ImageCreateInfo image_info = vk::ImageCreateInfo ()
      .setImageType (vk::ImageType::e2D)
      .setMipLevels (mip_levels)
      .setArrayLayers (1)
      .setFormat (format)
      .setTiling (tiling)
      .setInitialLayout (vk::ImageLayout::eUndefined)
      .setUsage (usage)
      .setSharingMode (vk::SharingMode::eExclusive)
      .setSamples (num_samples)
      .setExtent(vk::Extent3D ()
		 .setWidth (width)
		 .setHeight (height)
		 .setDepth (1));

    if (m_logical_device.createImage (&image_info, nullptr, &image) != vk::Result::eSuccess)
      {
	throw std::runtime_error("Failed to create image.");
      }

    vk::MemoryRequirements mem_requirements;
    m_logical_device.getImageMemoryRequirements (image, &mem_requirements);

    vk::MemoryAllocateInfo alloc_info = vk::MemoryAllocateInfo ()
      .setAllocationSize (mem_requirements.size)
      .setMemoryTypeIndex (m_find_memory_type (mem_requirements.memoryTypeBits, properties));

    if (m_logical_device.allocateMemory (&alloc_info, nullptr, &image_memory) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to allocate image memory!");
      }

    m_logical_device.bindImageMemory (image, image_memory, 0);
  }
  
  void
  render::m_draw_frame ()
  {
    static_cast<std::any> (m_logical_device.waitForFences (1, &m_in_flight_fences[m_current_frame], VK_TRUE, std::numeric_limits<uint>::max()));
  
    vk::Result result = m_logical_device.acquireNextImageKHR (m_swap_chain, std::numeric_limits<uint>::max(), m_image_available_semaphores[m_current_frame], nullptr, &m_image_index);

    if (result == vk::Result::eErrorOutOfDateKHR or result == vk::Result::eSuboptimalKHR)
      {
	m_recreate_swap_chain ();
	return;
      }
    else if (result != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to acquire swap chain image!");
      }

    if (m_images_in_flight[m_image_index])
      {
	static_cast<std::any> (m_logical_device.waitForFences (1, &m_images_in_flight[m_image_index], VK_TRUE, std::numeric_limits<uint>::max()));
      }
    m_images_in_flight[m_image_index] = m_in_flight_fences[m_current_frame];

    vk::Semaphore wait_semaphores[] = {m_image_available_semaphores[m_current_frame]};
    vk::Semaphore signal_semaphores[] = {m_render_finished_semaphores[m_current_frame]};
    vk::PipelineStageFlags wait_stages[] = {vk::PipelineStageFlagBits::eColorAttachmentOutput};
    
    vk::SubmitInfo submit_info = vk::SubmitInfo ()
      .setWaitSemaphoreCount (1)
      .setPWaitSemaphores (wait_semaphores)
      .setPWaitDstStageMask (wait_stages)
      .setCommandBufferCount (1)
      .setPCommandBuffers (&m_command_buffers[m_image_index])
      .setSignalSemaphoreCount (1)
      .setPSignalSemaphores (signal_semaphores);

    static_cast<std::any> (m_logical_device.resetFences (1, &m_in_flight_fences[m_current_frame]));

    if (m_graphics_queue.submit (1, &submit_info, m_in_flight_fences[m_current_frame]) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to submit draw command buffer.");
      }

    vk::SwapchainKHR swap_chains[] = {m_swap_chain};
  
    vk::PresentInfoKHR present_info = vk::PresentInfoKHR ()
      .setWaitSemaphoreCount (1)
      .setPWaitSemaphores (signal_semaphores)
      .setSwapchainCount (1)
      .setPSwapchains (swap_chains)
      .setPImageIndices (&m_image_index);

    static_cast<std::any> (m_present_queue.presentKHR (&present_info));

    m_present_queue.waitIdle ();

    m_current_frame = (m_current_frame + 1) % m_max_frames_in_flight;
  }

  queue_family_indices
  render::m_find_queue_families ()
  {
    queue_family_indices indices;

    std::vector<vk::QueueFamilyProperties> queue_families = m_physical_device.getQueueFamilyProperties ();

    int i = 0;
    for (const auto& queue_family : queue_families)
      {
	if (queue_family.queueFlags & vk::QueueFlagBits::eGraphics)
	  {
	    indices.graphics_family = i;
	  }

	vk::Bool32 present_support = false;
	static_cast<std::any> (m_physical_device.getSurfaceSupportKHR (i, m_surface, &present_support));

	if (present_support)
	  {
	    indices.present_family = i;
	  }

	if (indices.is_complete ())
	  {
	    break;
	  }
      
	++i;
      }
  
    return indices;
  }
  
  void
  render::m_update_uniform_buffer (uint32_t current_image)
  {     
    m_ubo.view = glm::lookAt (m_camera_pos,
			    m_camera_pos + m_camera_front,
			    m_camera_up);

    void* data;
    
    vkMapMemory (m_logical_device, m_uniform_buffers_memory[current_image], 0, sizeof (m_ubo), 0, &data);
    memcpy (data, &m_ubo, sizeof (m_ubo));
    m_logical_device.unmapMemory (m_uniform_buffers_memory[current_image]);    
  }
 
  void
  render::m_generate_mipmaps (vk::Image image, vk::Format image_format, int32_t tex_width, int32_t tex_height, uint32_t mip_levels)
  {
    vk::FormatProperties format_properties;
    m_physical_device.getFormatProperties (image_format, &format_properties);

    if (!(format_properties.optimalTilingFeatures & vk::FormatFeatureFlagBits::eSampledImageFilterLinear))
      {
	throw std::runtime_error ("Texture image format does not support linear blitting.");
      }
  
    vk::CommandBuffer command_buffer = m_begin_single_time_commands ();

    vk::ImageSubresourceRange subres = vk::ImageSubresourceRange ()
      .setAspectMask (vk::ImageAspectFlagBits::eColor)
      .setBaseArrayLayer (0)
      .setLayerCount (1)
      .setLevelCount (1);

    vk::ImageMemoryBarrier barrier = vk::ImageMemoryBarrier ()
      .setImage (image)
      .setSrcQueueFamilyIndex (VK_QUEUE_FAMILY_IGNORED)
      .setDstQueueFamilyIndex (VK_QUEUE_FAMILY_IGNORED);
    

    int32_t mip_width = tex_width;
    int32_t mip_height = tex_height;

    for (uint32_t i = 1; i < mip_levels; ++i)
      {
	barrier
	  .setSubresourceRange (subres.setBaseMipLevel (i - 1))
	  .setOldLayout (vk::ImageLayout::eTransferDstOptimal)
	  .setNewLayout (vk::ImageLayout::eTransferSrcOptimal)
	  .setSrcAccessMask (vk::AccessFlagBits::eTransferWrite)
	  .setDstAccessMask (vk::AccessFlagBits::eTransferRead);

	command_buffer.pipelineBarrier (vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eTransfer, vk::DependencyFlags (0),
					0, nullptr,
					0, nullptr,
					1, &barrier);

	// Souce offsets:
	std::array<vk::Offset3D, 2> src_offsets;
	src_offsets[0] = vk::Offset3D (0, 0, 0);
	src_offsets[1] = vk::Offset3D (mip_width, mip_height, 1);

	// Desination offsets:
	std::array<vk::Offset3D, 2> dst_offsets;
	dst_offsets[0] = vk::Offset3D (0, 0, 0);
	dst_offsets[1] = vk::Offset3D (mip_width > 1 ? mip_width / 2 : 1, mip_height > 1 ? mip_height / 2 : 1, 1);
      
	vk::ImageBlit blit = vk::ImageBlit ()
	  .setSrcOffsets (src_offsets)
	  .setSrcSubresource (vk::ImageSubresourceLayers ()
			      .setAspectMask (vk::ImageAspectFlagBits::eColor)
			      .setMipLevel (i - 1)
			      .setBaseArrayLayer (0)
			      .setLayerCount (1))
	  .setDstOffsets (dst_offsets)
	  .setDstSubresource (vk::ImageSubresourceLayers ()
			      .setAspectMask (vk::ImageAspectFlagBits::eColor)
			      .setMipLevel (i)
			      .setBaseArrayLayer (0)
			      .setLayerCount (1));

	command_buffer.blitImage (image, vk::ImageLayout::eTransferSrcOptimal,
				  image, vk::ImageLayout::eTransferDstOptimal,
				  1, &blit,
				  vk::Filter::eLinear);

	barrier
	  .setOldLayout (vk::ImageLayout::eTransferSrcOptimal)
	  .setNewLayout (vk::ImageLayout::eShaderReadOnlyOptimal)
	  .setSrcAccessMask (vk::AccessFlagBits::eTransferRead)
	  .setDstAccessMask (vk::AccessFlagBits::eShaderRead);

	command_buffer.pipelineBarrier (vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, vk::DependencyFlags (0),
					0, nullptr,
					0, nullptr,
					1, &barrier);
      
	if (mip_width > 1)
	  {
	    mip_width /= 2;
	  }
	if (mip_height > 1)
	  {
	    mip_height /= 2;
	  }
      }

    barrier
      .setSubresourceRange (subres.setBaseMipLevel (mip_levels - 1))
      .setOldLayout (vk::ImageLayout::eTransferDstOptimal)
      .setNewLayout (vk::ImageLayout::eShaderReadOnlyOptimal)
      .setSrcAccessMask (vk::AccessFlagBits::eTransferWrite)
      .setDstAccessMask (vk::AccessFlagBits::eShaderRead);

    command_buffer.pipelineBarrier (vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, vk::DependencyFlags (0),
				    0, nullptr,
				    0, nullptr,
				    1, &barrier);

    m_end_single_time_commands (command_buffer);
  }
  
  vk::SampleCountFlagBits
  render::m_get_max_usable_sample_count ()
  {
    vk::PhysicalDeviceProperties physical_device_properties = m_physical_device.getProperties ();

    vk::SampleCountFlags counts = physical_device_properties.limits.framebufferColorSampleCounts & physical_device_properties.limits.framebufferDepthSampleCounts;

    if (counts & vk::SampleCountFlagBits::e64) {return vk::SampleCountFlagBits::e64;}
    if (counts & vk::SampleCountFlagBits::e32) {return vk::SampleCountFlagBits::e32;}
    if (counts & vk::SampleCountFlagBits::e16) {return vk::SampleCountFlagBits::e16;}
    if (counts & vk::SampleCountFlagBits::e8) {return vk::SampleCountFlagBits::e8;}
    if (counts & vk::SampleCountFlagBits::e4) {return vk::SampleCountFlagBits::e4;}
    if (counts & vk::SampleCountFlagBits::e2) {return vk::SampleCountFlagBits::e2;}

    return vk::SampleCountFlagBits::e1;
  }
  
  void
  render::m_create_color_resources ()
  {
    vk::Format color_format = m_swap_chain_image_format;

    m_create_image (m_swap_chain_extent.width,
		    m_swap_chain_extent.height,
		    1, m_msaa_samples,
		    color_format,
		    vk::ImageTiling::eOptimal,
		    vk::ImageUsageFlagBits::eTransientAttachment |
		    vk::ImageUsageFlagBits::eColorAttachment,
		    vk::MemoryPropertyFlagBits::eDeviceLocal,
		    m_color_image,
		    m_color_image_memory);

  m_color_image_view = m_create_image_view (m_color_image, color_format, vk::ImageAspectFlagBits::eColor, 1);
  }
  
  void
  render::m_copy_buffer_to_image (vk::Buffer buffer,
				  vk::Image image,
				  uint32_t width,
				  uint32_t height)
  {
    vk::CommandBuffer command_buffer = m_begin_single_time_commands ();

    vk::BufferImageCopy region = vk::BufferImageCopy ()
      .setBufferOffset (0)
      .setBufferRowLength (0)
      .setBufferImageHeight (0)
      .setImageOffset ({0, 0, 0})
      .setImageExtent ({width, height, 1})
      .setImageSubresource (vk::ImageSubresourceLayers ()
			    .setAspectMask (vk::ImageAspectFlagBits::eColor)
			    .setMipLevel (0)
			    .setBaseArrayLayer (0)
			    .setLayerCount (1));

    command_buffer.copyBufferToImage (buffer, image, vk::ImageLayout::eTransferDstOptimal, 1, &region);
  
    m_end_single_time_commands (command_buffer);
  }
  
  void
  render::m_create_buffer (vk::DeviceSize size,
			   vk::BufferUsageFlags usage,
			   vk::MemoryPropertyFlags properties,
			   vk::Buffer& buffer,
			   vk::DeviceMemory& buffer_memory)
  {
    vk::BufferCreateInfo buffer_info = vk::BufferCreateInfo ()
      .setSize (size)
      .setUsage (usage)
      .setSharingMode (vk::SharingMode::eExclusive);

    if (m_logical_device.createBuffer (&buffer_info, nullptr, &buffer) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to create buffer!");
      }

    vk::MemoryRequirements mem_requirements;
    m_logical_device.getBufferMemoryRequirements (buffer, &mem_requirements);

    vk::MemoryAllocateInfo alloc_info = vk::MemoryAllocateInfo ()
      .setAllocationSize (mem_requirements.size)
      .setMemoryTypeIndex (m_find_memory_type (mem_requirements.memoryTypeBits, properties));

    if (m_logical_device.allocateMemory (&alloc_info, nullptr, &buffer_memory) != vk::Result::eSuccess)
      {
	throw std::runtime_error ("Failed to allocate buffer memory!");
      }

    m_logical_device.bindBufferMemory (buffer, buffer_memory, 0);
  }

  
  void
  render::m_copy_buffer (vk::Buffer src_buffer,
			 vk::Buffer dst_buffer,
			 vk::DeviceSize size)
  {
    vk::CommandBuffer command_buffer = m_begin_single_time_commands ();

    vk::BufferCopy copy_region = vk::BufferCopy ().setSize (size);
    command_buffer.copyBuffer (src_buffer, dst_buffer, 1, &copy_region);

    m_end_single_time_commands (command_buffer);
  }
  
  void
  render::m_main_loop ()
  {
    // Make sure image index isn't 0 before mapping out the memory on the logical device, otherwise we will get a segmentation fault.
    while ((m_image_index = 0)) {};
    
    double
      current_time = glfwGetTime (),
      accumulator = 0.0;

    std::jthread constant_update_thread ([&] ()
					 {
					   while (!glfwWindowShouldClose (m_window))
					     {
					       m_get_cursor_input ();
					     }
					 });
   
    std::jthread render_thread ([&] ()
				{ 
				  while (!glfwWindowShouldClose (m_window))
				    {
				      double new_time = glfwGetTime ();
				      if ((new_time - current_time) > 0.033)
					{
					  m_delta_time = 0.033;
					}
				      else
					{
					  m_delta_time = new_time - current_time;
					}
				      current_time = new_time;
				      accumulator += m_time_step;

				      m_draw_frame ();
				    }
				});

    std::jthread update_uniform_buffer_thread ([&] ()
					       { 
						 m_ubo.model = glm::translate(glm::mat4 (1.0f),
									    glm::vec3 (0.0f, 0.0f, 0.0f));

						 m_ubo.proj = glm::perspective (glm::radians (70.0f),
									      m_swap_chain_extent.width / static_cast<float> (m_swap_chain_extent.height),
									      0.001f,
									      std::numeric_limits<float>::max());
						 // Flip image.
						 m_ubo.proj[1][1] *= -1;
						 
						 while (!glfwWindowShouldClose (m_window))
						   {
						     m_update_uniform_buffer (m_image_index);
						   }
					       });
  
    while (!glfwWindowShouldClose (m_window))
      {
	while (accumulator >= m_time_step)
	  {
	    m_get_key_input ();
	    accumulator -= m_time_step;
	  }
	glfwPollEvents ();
      }
  }
} // namesapce zelus

