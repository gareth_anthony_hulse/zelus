/* © Copyright 2020
 * Gareth Anthony Hulse
 *
 * SPDX-License-Identifier: FSFAP
 */

#version 460
#extension GL_ARB_separate_shader_objects : enable

layout (binding = 1) uniform sampler2D tex_smapler;

layout (location = 0) in vec3 frag_color;
layout (location = 1) in vec2 frag_tex_coord;

layout (location = 0) out vec4 out_color;

void
main ()
{
  out_color = vec4 (frag_color * texture (tex_smapler, frag_tex_coord).rgb, 1.0);
}
